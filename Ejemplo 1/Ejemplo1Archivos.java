// Creacion de archivo en java
/* FileWriter se utiliza para escribir informacion en un archivo 
   PrintWriter  que es la que creara el archivo en disco duro

*/

import java.io.*;

// crear un archivo
public class Ejemplo1Archivos{

 public static void main(String args[]){
 
   File archivo = new File("..//archivo1.txt");
   
    try{
      PrintWriter salida = new PrintWriter(new FileWriter(archivo));
      salida.close();
    
    }catch(IOException ex){
     ex.printStackTrace();
    
    }
    
    System.out.println("El archivo se ha creado correctamente");
 }

}