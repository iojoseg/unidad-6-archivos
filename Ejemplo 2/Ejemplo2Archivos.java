// FileReader  se utiliza para leer caracteres
 
import java.io.*;

// lectura de un archivo
public class Ejemplo2Archivos{

   public static void main(String args[]){
   
     File archivo = new File("..//archivo1.txt");
     
     try{
       BufferedReader entrada = new BufferedReader(new FileReader(archivo));
       String lectura = entrada.readLine();
       while(lectura != null){
         System.out.println("Lectura: " + lectura);
         lectura = entrada.readLine();
       }
       entrada.close();
     
     }catch(FileNotFoundException ex){
     ex.printStackTrace();
     }catch(IOException ex){
     ex.printStackTrace();
     }
   
   }

}