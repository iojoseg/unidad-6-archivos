import java.io.*;

public class Ejemplo3Archivos{
  public static void main(String args[]){
  
    File archivo = new File("..//archivo2.txt");
    
    try{
       PrintWriter salida = new PrintWriter(new FileWriter(archivo,false));
       String contenido = "Escribir Contendido del archivo 2.2 ";
       salida.println(contenido);
       salida.println("Fin de archivo2");
       salida.println("Agrego otra linea2");
       salida.close();
      
    }catch(IOException ex){
     ex.printStackTrace();
    }
    System.out.println("Se envio informacion al archivo2");
  }

}